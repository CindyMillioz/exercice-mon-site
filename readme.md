# Projet - Mon site

## Tech
 - HTML
 - CSS

 ## Architecture
 - index.html
 - contact.html
 - produit.html
 - readme.md
    - images 
        - 1.jpg
        - 2.jpg
        - 3.jpg
        - 4.jpg
        - 5.jpg
        - 6.jpg
        - 7.jpg
        - 8.jpg
    - CSS
        - style.css
        - produit.css
        - contact.css
        - responsive.css

## Consignes
- Réaliser un site en html css
- Faire une bonne architecture de projet
- Réliser des positionnements sur plusieurs éléments
- Réaliser un site responsive

